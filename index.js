const HTTP = require("http");
let myPort = 3000

HTTP.createServer((request,response)=>{

    if (request.url===`/login`){
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.write(`You are in the login page.`);
        response.end();
    } else {
        response.writeHead(404, {"Content-Type": "text/plain"});
        response.write(`Page does not exist.`);
        response.end();
    }

}).listen(myPort);

console.log(`Server successfully running`);
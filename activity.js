//Q: What directive is used by Node.js in loading the modules it needs?
//A: nodemon

//Q:What Node.js module contains a method for server creation?
//A: HTTP

//Q: What is the method of the http object responsible for creating a server using Node.js?
//A: HTTP.CreateServer()


//Q: What method of the response object allows us to set status codes and content types?
//A: response.writeHeader()


//Q: Where will console.log() output its contents when run in Node.js?
//A: server/ terminal

//Q: What property of the request object contains the address' endpoint?
//A: request.url